package com.example.englisharticles;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_CODE_PERMISSION_WRITE_EXTERNAL_STORAGE = 1;
    TextView textArticles;
    TextView textTask;
    Button buttonStartTest;
    Button buttonNext;
    Button buttonA;
    Button buttonAn;
    Button buttonThe;
    Button buttonEmpty;
    Button buttonBack;
    AlertDialog.Builder ad;
    String Rule;
    boolean ruleShow = true;
    public String ArtcleFileId;
    MyDatabaseHelper dbHelper;
    int countWrongAnswer = 0;
    public int countRows;
    int countTruthAnswer = 0;
    SQLiteDatabase db;
    Cursor query;



    public void setBorderCorrectAnswer() {
        if (ArtcleFileId.equals("the")) {
            buttonThe.setBackgroundResource(R.drawable.green_border);
        } else if (ArtcleFileId.equals("a")) {
            buttonA.setBackgroundResource(R.drawable.green_border);

        } else if (ArtcleFileId.equals("an")) {
            buttonAn.setBackgroundResource(R.drawable.green_border);

        } else if (ArtcleFileId.equals("empty")) {
            buttonEmpty.setBackgroundResource(R.drawable.green_border);

        }

    }

    public void setStateButton(boolean state) {
        if (state) {
            buttonA.setEnabled(true);
            buttonThe.setEnabled(true);
            buttonAn.setEnabled(true);
            buttonEmpty.setEnabled(true);

        } else {
            buttonA.setEnabled(false);
            buttonThe.setEnabled(false);
            buttonAn.setEnabled(false);
            buttonEmpty.setEnabled(false);
        }
    }

    public void endTest() {
        int countRows = query.getCount();
        if (query.getPosition() + 1 == countRows) //сравниваем с позицией курсора +1 т.к как позиция курсора начинается с нуля
        {
            query.close();
            db.close();
            buttonStartTest.setEnabled(true);
            buttonNext.setEnabled(false);
            ShowResult();
        }
    }

    public void setColorBorderElements() {
        textArticles.setBackgroundResource(R.drawable.border);
        buttonEmpty.setBackgroundResource(R.drawable.border);
        buttonThe.setBackgroundResource(R.drawable.border);
        buttonA.setBackgroundResource(R.drawable.border);
        buttonAn.setBackgroundResource(R.drawable.border);
    }

    public void ShowResult() {
        countRows = query.getCount();
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("");
        alertDialog.setMessage("Процент правильных неправильных ответов: " + countTruthAnswer * 100 / countRows + "%");

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Close",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Start Test Again",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        buttonStartTest.performClick();
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

        Button btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        Button btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) btnPositive.getLayoutParams();
        layoutParams.weight = 10;
        btnPositive.setLayoutParams(layoutParams);
        btnNegative.setLayoutParams(layoutParams);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonA:
                if (ArtcleFileId.equals("a")) {
                    setBorderCorrectAnswer();

                    ruleShow = true;
                    setBorderCorrectAnswer();
                    countTruthAnswer++;
                    endTest();
                    textArticles.setBackgroundResource(R.drawable.green_border);
                } else {

                    ruleShow = false;
                    textTask.setText(Rule);
                    textArticles.setBackgroundResource(R.drawable.red_border);
                    buttonA.setBackgroundResource(R.drawable.red_border);
                    setBorderCorrectAnswer();
                    countWrongAnswer++;
                    endTest();

                }
                setStateButton(false);
                break;
            case R.id.buttonAn:
                if (ArtcleFileId.equals("an")) {
                    setBorderCorrectAnswer();
                    ruleShow = true;
                    setBorderCorrectAnswer();
                    countTruthAnswer++;
                    endTest();
                    textArticles.setBackgroundResource(R.drawable.green_border);
                } else {
                    ruleShow = false;
                    textTask.setText(Rule);
                    textArticles.setBackgroundResource(R.drawable.red_border);
                    buttonAn.setBackgroundResource(R.drawable.red_border);
                    setBorderCorrectAnswer();
                    countWrongAnswer++;
                    endTest();
                }
                setStateButton(false);
                break;
            case R.id.buttonThe:
                if (ArtcleFileId.equals("the")) {
                    setBorderCorrectAnswer();
                    ruleShow = true;
                    countTruthAnswer++;
                    textArticles.setBackgroundResource(R.drawable.green_border);
                    endTest();
                } else {
                    ruleShow = false;
                    textTask.setText(Rule);
                    textArticles.setBackgroundResource(R.drawable.red_border);
                    buttonThe.setBackgroundResource(R.drawable.red_border);
                    setBorderCorrectAnswer();
                    countWrongAnswer++;
                    endTest();
                }
                setStateButton(false);
                break;
            case R.id.buttonEmpty:
                if (ArtcleFileId.equals("empty")) {
                    setBorderCorrectAnswer();

                    ruleShow = true;

                    countTruthAnswer++;
                    textArticles.setBackgroundResource(R.drawable.green_border);
                    endTest();
                } else {
                    ruleShow = false;
                    textTask.setText(Rule);
                    textArticles.setBackgroundResource(R.drawable.red_border);
                    buttonEmpty.setBackgroundResource(R.drawable.red_border);

                    setBorderCorrectAnswer();
                    countWrongAnswer++;
                    endTest();
                }
                setStateButton(false);
                break;
            case R.id.StartTest:
                try {
                    setColorBorderElements();
                    setStateButton(true);
                    ruleShow = true;
                    textArticles.setText("");
                    countTruthAnswer = 0;
                    buttonNext.setEnabled(true);


                    // query = db.rawQuery("SELECT * FROM TaskTable;", null);

                    db = dbHelper.getWritableDatabase();
                    query = db.rawQuery("SELECT * FROM TaskTable ORDER BY RANDOM() LIMIT 20", null);
                    // int random_number1 = 0 + (int) (Math.random() * countRows);
                    query.moveToFirst();
                    int id = query.getInt(0);
                    String Article = query.getString(1);
                    String Task = query.getString(2);
                    Rule = query.getString(3);

                    ArtcleFileId = Article;
                    textArticles.setText(Task);


                    buttonStartTest.setEnabled(false);

                } catch (Exception e) {
                    textTask.setText(e.toString());
                }
                break;
            case R.id.buttonNext:
                try {
                    textTask.setText("");
                    textArticles.setText("");
                    setColorBorderElements();
                    setStateButton(true);
                    ArtcleFileId = "";

                    textArticles.setText(String.valueOf(query.getCount()));

                    query.moveToNext();


                    String Article = query.getString(1);
                    String Task = query.getString(2);
                    Rule = query.getString(3);

                    ArtcleFileId = Article;
                    textArticles.setText(Task);
                    // functionEndTest();
                    if (query.isLast()) {
                        buttonNext.setEnabled(false);
                    }


                } catch (Exception e) {
                    textTask.setText(e.toString());
                }

                break;


        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        // checkRequestPermission();
        textArticles = (TextView) findViewById(R.id.Task);
        textTask = (TextView) findViewById(R.id.Answer);
        buttonA = (Button) findViewById(R.id.buttonA);
        buttonAn = (Button) findViewById(R.id.buttonAn);
        buttonThe = (Button) findViewById(R.id.buttonThe);
        buttonEmpty = (Button) findViewById(R.id.buttonEmpty);

        buttonStartTest = (Button) findViewById(R.id.StartTest);
        buttonNext = (Button) findViewById(R.id.buttonNext);


// устанавливаем один обработчик для всех кнопок
        buttonA.setOnClickListener(this);
        buttonAn.setOnClickListener(this);
        buttonThe.setOnClickListener(this);
        buttonEmpty.setOnClickListener(this);
        buttonNext.setOnClickListener(this);
        buttonStartTest.setOnClickListener(this);
        buttonStartTest.setOnClickListener(this);
        dbHelper = new MyDatabaseHelper(this);


        buttonNext.setEnabled(false);
        setStateButton(false);

    }


}
