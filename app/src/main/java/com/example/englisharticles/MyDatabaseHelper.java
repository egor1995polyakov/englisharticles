package com.example.englisharticles;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.SQLException;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.database.sqlite.SQLiteDatabase;




public class MyDatabaseHelper extends SQLiteOpenHelper {

  private static final String DATABASE_NAME = "DBTask";

  private static final int DATABASE_VERSION = 2;

  // Database creation sql statement
  private static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS " + "TaskTable (id integer primary key autoincrement,Article TEXT , Task TEXT NOT NULL UNIQUE ,Rule TEXT)";

  public MyDatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  // Method is called during creation of the database
  @Override
  public void onCreate(SQLiteDatabase database) {
    database.execSQL(DATABASE_CREATE);
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('a', '___ Maria called you(некая Мария).','Артикль a т.к контексте не понимаем какая именно Мария звонила.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('the', '___ weather is sunny.','Уникальная сущность в этом мире.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('the', 'I went to home in ___ morning.','Часть дня.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'They play ___ chess for money.','В игровых видах спорта артикль не ставится.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('the', 'She is  in ___ corner.','Артикль the показывает местоположение.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('the', 'This is ___ Maria,who I met yesterday.','Артикль the показывает, что это конкретный человек.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('a', 'This is ___ cat.','Артикль a показывает, что мы говорим о предмете впервые.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('a', 'He speaks in ___ loud voice.','Устойчивое выражение.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('an', 'He is ___ engineer.','Артикль an, неопределенный артикль перед проффесиями, которые начинаются с гласной.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'He ate ___ bread.','Артикль не ставиться, bread - несчетное существительное.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('a', 'It looked at the wolf and said in ___ thin voice.','Артикль a ставиться, in a thin voice устойчивое выражение.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'He began to shout ''You ___ fool, you are making the water dirty''!','Артикль в обращении не ставится.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'Whoever killed her must have stayed on ___ board...','Артикль не ставится в устойчивом выражении: on board.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'In ___ conclusion, I wish everyone every success in this new endeavour.','Артикль не ставится в устойчивом выражении: In conclusion.');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'I beat Federer four times at ___ Wimbledon.','Артикль не ставиться так как Wimbledon - исключение');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'He looked at her from ___ head to ___ foot.','Артикль не ставиться так как from head to foot - устойчивое выражение');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'Everybody on ___ board.','Артикль не ставиться так как on board - устойчивое выражение');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'I am working two jobs, I am drowning in ___ debt ','Артикль не ставиться так как on board - устойчивое выражение');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'He does nothing but complain from ___ morning till ___ night','Артикль не ставиться так как from morning till night - устойчивое выражение');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('a', 'Such ___ beatiful girl','Артикль a Когда идет восклицание со словами Such и What');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('empty', 'After school I want to go to ___ university ','Не ставиться артикль, так как тут мы имеем в виду не здание, а то что хотим стать студентом ');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('the', 'Excuse me, how I can get to ___ university ','Cтавиться артикль the, так как тут мы имеем в виду  здание, а не то что мы  хотим стать студентом ');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('the', 'The architects went to ___ church to plan the redecoration ','Cтавиться артикль the, так как тут мы рассматриваем церковь как здание а не предмет хождения туда на протяжении длительного времени ');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('a', 'They spoke in ___ weak voice ','Устойчивое выражение in a weak voice ');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('the', 'He gave up play ___ violin three years ago ','С музыкальными инструментами ставиться артикль the ');");
    database.execSQL("INSERT INTO TaskTable (Article,Task,Rule) VALUES ('the', 'Geogragpy is ___ most interesting subject at school',' The Most обычно используется в функции превосходной степени, и ставится перед прилагательными, состоящими из двух и более слогов ');");

}

  // Method is called during an upgrade of the database,
  @Override
  public void onUpgrade(SQLiteDatabase database,int oldVersion,int newVersion){

    database.execSQL("DROP TABLE IF EXISTS MyEmployees");
    onCreate(database);
  }
}

